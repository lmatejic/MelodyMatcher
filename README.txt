MelodyMatcher - A basic genetic algorithm that matches a given melody

This simple program was created to explore the basic abilities of JGAP. The program uses no over-complicated Java code, and contains comments on pretty much everything that is happening throughout the program. The program was made as an Eclipse project, so use an Eclipse project compatible IDE, or use the provided source files in your favorite IDE/editor. Feel free to use the program if it serves your educational/recreational purposes. For additional information on JGAP/JFugue and/or licenses that apply to the libraries, visit the sites listed below. 

Required (included) library:

https://sourceforge.net/projects/jgap/ - JGAP- Java Genetic Algorithm Package - used under LGPL licence


Optional library for audio demonstration purposes:

http://www.jfugue.org/download.html - JFugue - Music Programming for Java™ and JVM Languages


Author: Luka Matejić, 2016.