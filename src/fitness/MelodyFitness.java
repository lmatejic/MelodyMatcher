package fitness;

import org.jgap.FitnessFunction;
import org.jgap.IChromosome;

import model.Melody;

public class MelodyFitness extends FitnessFunction{
	
	private static final long serialVersionUID = -7885392929983150463L;

	Melody targetMelody;
	int melodySize;
	
	public MelodyFitness(Melody targetMelody, int melodySize) {
		this.targetMelody = targetMelody;
		this.melodySize = melodySize;
	}

	@Override
	protected double evaluate(IChromosome melodyChromosome) {
		
		int positionScore = calculateNotePositionScore(melodyChromosome, targetMelody);
		int valueDifference = calculateValueDifference(melodyChromosome, targetMelody);
		
		//a fitness function's evaluation return value may not drop below 0
		return positionScore - valueDifference > 0 ? positionScore - valueDifference : 0;
	}

	//Give an initial score to a chromosome by comparing the value of a musical note at a certain position
	//to a note belonging to targetMelody at the same position, then multiply by 10^n
	//to prepare the score for the next step and to provide better score visualisation.
	private int calculateNotePositionScore(IChromosome melodyChromosome, Melody targetMelody) {
		int score = 0;
		
		for(int i=0; i < melodySize; i++) {
				if(targetMelody.getMelody()[i] == (char)(int)melodyChromosome.getGene(i).getAllele())
					score++;
			}
		
		return score * 100;
	}
	
	//Get the sums of ASCII values from the target and current candidate melody,
	//then return the absolute value of their difference.
	//With this step we provide a method of differentiation between chromosomes
	//which have the same initial(note position-wise) score and are at first glance equally qualified candidates.
	//However, some chromosomes may have a bigger gap between the notes different from the targetMelody's notes at the respective positions,
	//this step will differentiate the good melodies from the even better ones.
	private int calculateValueDifference(IChromosome melodyChromosome, Melody targetMelody) {
		char[] currentMelody = new char[melodySize];
		for(int i = 0; i < melodySize; i++) {
			currentMelody[i] = (char)(int)melodyChromosome.getGene(i).getAllele();
		}
		return Math.abs(Melody.calculateMelodyValue(targetMelody.getMelody())- Melody.calculateMelodyValue(currentMelody));
	}
}
