package model;

import java.util.Scanner;

public class Melody {
	private char[] melodyArray;
	
	public Melody() {
		System.out.println("Enter a melody in English musical notation. Whitespace characters will be ignored.");
		Scanner scanner = new Scanner(System.in);
		String melodyString = new String();
		boolean inputWrong;
		do {
			System.out.println("The permitted letters range from A-G");
			inputWrong = false;
			melodyString = scanner.nextLine().toUpperCase().replaceAll("\\s", "");
			if(melodyString.matches(".*[^ABCDEFG].*")) {
				inputWrong = true;
			}
		} while(inputWrong);
		scanner.close();
		melodyArray = melodyString.toCharArray();
	}
	
	public char[] getMelody() {
		return this.melodyArray;
	}
	
	public void setMelody(char[] melodyArray) {
		this.melodyArray = melodyArray;
	}
	
	//calculate the ASCII value of a whole given melody
	public static int calculateMelodyValue(char[] melodyArray) {
		int value = 0;
		for(char c : melodyArray) {
			value+= c;
		}
		
		return value;
	}
}
