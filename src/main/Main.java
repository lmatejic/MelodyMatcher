package main;

import fitness.MelodyFitness;
import model.Melody;

public class Main {

	public static final int POPULATION_SIZE = 100;
	public static final int NUMBER_OF_EVOLUTIONS = 50;

	public static void main(String[] args) throws InvalidConfigurationException {
		Melody melody = new Melody();
		//Start timing after input.
		long startTime = System.currentTimeMillis();
		
		//Construct a MelodyFitness object, initialize the configuration object
		//and add some genetic operators of your choice.
		MelodyFitness melodyFitness = new MelodyFitness(melody, melody.getMelody().length);
		DefaultConfiguration conf = new DefaultConfiguration();
		CrossoverOperator xOperator = new CrossoverOperator(conf);
		GaussianMutationOperator mOperator = new GaussianMutationOperator(conf);

		//Put your configuration parameters into the configuration object.
		conf.setKeepPopulationSizeConstant(true);
		conf.setPopulationSize(POPULATION_SIZE);
		conf.setFitnessFunction(melodyFitness);
		conf.addGeneticOperator(xOperator);
		conf.addGeneticOperator(mOperator);
		
		//Fill the genes with ASCII values corresponding to their English musical notation counterparts (characters A-G).
		IntegerGene[] genes = new IntegerGene[melody.getMelody().length];
		for(int i = 0; i < melody.getMelody().length; i++) {
			genes[i] = new IntegerGene(conf, 65, 71);
		};
		
		//Construct a Chromosome object with the created configuration and genes,
		//afterwards passing it over to the configuration as a sample chromosome.
		Chromosome chromosome = new Chromosome(conf, genes);
		conf.setSampleChromosome(chromosome);
		
		//Retreive your population while simultaneously determining the initial genotype seed.
		Genotype population = Genotype.randomInitialGenotype(conf);
		
		//Iterate as many times as needed (or in this case as many times as determined by the constant),
		//do some work like retreiving the fittest chromosome, printing out some values, etc.
		for (int i = 0; i < NUMBER_OF_EVOLUTIONS; i++) {
			IChromosome bestSolutionSoFar =
			population.getFittestChromosome();
			
			printChromosome(bestSolutionSoFar, melody);
			
			//Check whether there is a solution, therefore a need to stop evolving and executing your genetic algorithm.
			if(checkEquality(bestSolutionSoFar, melody)){
				System.out.println("After " + i + " evolutions: ");
				printChromosome(bestSolutionSoFar, melody);
				System.out.println("The melody was found !");
				break;	
			}

			population.evolve();
		}
		
		//Stop the timer and print out the total algorithm execution time.
		long endTime = System.currentTimeMillis();
		System.out.println("Time total : "
				+ (((double) endTime - startTime) / 1000) + " seconds");
	}
	
	public static void printChromosome(IChromosome chromosome, Melody targetMelody) {
		System.out.print("Current best solution is: ");
		for(int i = 0; i < chromosome.size(); i++) {
			System.out.print((char)(int)chromosome.getGene(i).getAllele() + " ");
		}
		System.out.println("");
		System.out.println("The solution's fitness function is: " + chromosome.getFitnessValue());
		System.out.println("");
		System.out.println("The target melody is: ");
		for(int i = 0; i < targetMelody.getMelody().length; i++) {
			System.out.print(targetMelody.getMelody()[i] + " ");
		}
		System.out.println("");
		System.out.println("");
	}

	private static boolean checkEquality(IChromosome chromosome, Melody targetMelody) {
		char[] currentMelody = new char[chromosome.size()];
		for(int i = 0; i < chromosome.size(); i++) {
			currentMelody[i] = (char)(int)chromosome.getGene(i).getAllele();
		}
		
		for(int i = 0; i < chromosome.size(); i++) {
			if(currentMelody[i] != targetMelody.getMelody()[i]) {
				return false;
			}
		}
		return true;
	}
}
